/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ (() => {

eval("// include api for currency change\nconst api = \"https://api.exchangerate-api.com/v4/latest/USD\";\n\n// for selecting different controls\nvar search = document.querySelector(\".searchBox\");\nvar convert = document.querySelector(\".convert\");\nvar fromCurrecy = document.querySelector(\".from\");\nvar toCurrecy = document.querySelector(\".to\");\nvar finalValue = document.querySelector(\".finalValue\");\nvar finalAmount = document.getElementById(\"finalAmount\");\nvar resultFrom;\nvar resultTo;\nvar searchValue;\n\n// Event when currency is changed\nfromCurrecy.addEventListener('change', event => {\n  resultFrom = `${event.target.value}`;\n});\n\n// Event when currency is changed\ntoCurrecy.addEventListener('change', event => {\n  resultTo = `${event.target.value}`;\n});\nsearch.addEventListener('input', updateValue);\n\n// function for updating value\nfunction updateValue(e) {\n  searchValue = e.target.value;\n}\n\n// when user clicks, it calls function getresults\nconvert.addEventListener(\"click\", getResults);\n\n// function getresults\nfunction getResults() {\n  fetch(`${api}`).then(currency => {\n    return currency.json();\n  }).then(displayResults);\n}\n\n// display results after convertion\nfunction displayResults(currency) {\n  let fromRate = currency.rates[resultFrom];\n  let toRate = currency.rates[resultTo];\n  finalValue.innerHTML = (toRate / fromRate * searchValue).toFixed(2);\n  finalAmount.style.display = \"block\";\n}\n\n// when user click on reset button\nfunction clearVal() {\n  window.location.reload();\n  document.getElementsByClassName(\"finalValue\").innerHTML = \"\";\n}\n;\n\n//# sourceURL=webpack://cicd-pipeline/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/index.js"]();
/******/ 	
/******/ })()
;